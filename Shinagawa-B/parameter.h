#define MAX_VAL 63         // LED の明るさ（0 から 255 の うち 2^n-1 のもの）
#define LEDn 60            // 光らせる LED の個数
#define DELAY_TIME 100000  // LED の点滅周期

#define MOTOR_SPEED 100    // モータスピード
