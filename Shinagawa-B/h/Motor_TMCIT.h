#include "Grove_I2C_Motor_Driver.h"
#define I2C_ADDRESS 0x0f // モータドライバはI2Cバスに接続。ディップスイッチは全て上のまま(0x0f)

void initMotor() {
  Motor.begin(I2C_ADDRESS);
}
