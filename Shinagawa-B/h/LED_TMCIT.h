/*macro definitions of PIR motion sensor pin and LED pin*/
#include <Adafruit_NeoPixel.h>

#define LEDpin 6
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LEDn, LEDpin, NEO_GRB + NEO_KHZ800);

int y = 0;     // LED の輝度
int count = 1; // 点滅コントロール用カウンタ

void initLED() {
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

void colorWipe(uint32_t c) {
  for (uint16_t i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
  }
  strip.show();
  // もっと周期を遅くしたければここにディレイを入れても良い。
//  delay(10);
}
