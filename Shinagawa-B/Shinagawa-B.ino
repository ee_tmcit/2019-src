#include "parameter.h"
#include "h/LED_TMCIT.h"
#include "h/PIR_TMCIT.h"
#include "h/Motor_TMCIT.h"


/* 回路接続
 * 　　人感センサ：
 *           Grove D2端子
 * 
 * 　　モータドライバ：
 * 　　　　　　　　　I2C：Arduino Grove I2C端子
 * 　　　　　　　　　M1+：DCモータ
 * 　　　　　　　　　M1-：DCモータ
 * 　　　　　　　　　外部電源用VS：外部電源3V
 * 　　　　　　　　　外部電源用GND：外部電源GND
 * 
 * 　　　　　　モータ：モータドライバの M1+, M1-
 * 
 * 　　　　　外部電源：5Vと3Vが必要（レギュレータで）
 * 
 * 　　LEDテープ：
 * 　　　　　　　　　GND：外部電源のGND
 * 　　　　　　　　　D：arduino D6端子
 *                5V：外部電源の5V
 */



/* 動作概要
 *  
 *  人感センサが反応したら、LEDが点滅し、モータが回転する。
 *  人感センサが反応しなくなったら、LEDが消灯し、モータが停止する。
 * 
 */


 
/* 動作パラメータ
 *  
 *  #define MAX_VAL 63         // LED の明るさ（0 から 255 の うち 2^n-1 のもの）
 *  #define LEDn 60            // 光らせる LED の個数
 *  #define DELAY_TIME 100000  // LED の点滅周期
 *  #define MOTOR_SPEED 100    // モータスピード
 *   
 *   これらを変更するにはファイル parameter.h を編集せよ。
 */ 


void setup() {
  initMotor();
  initLED();
  initPIR();
}

void loop() {
  if (digitalRead(PIR_MOTION_SENSOR)) {  // 人感センサーが反応している時の処理
    count--;
    if (count == 0) {                          // count が 0 になったら
      Motor.speed(MOTOR1, MOTOR_SPEED);        //       モータを回転させる
      y ^= MAX_VAL;                            //       y=LEDの明るさ；明るさを反転させている
      colorWipe(strip.Color(y, y, y));
      count = DELAY_TIME;
    }
  } else {                               //人感センサーが反応していない時の処理
    Motor.stop(MOTOR1);                        //       モータを停止させる
    y = 0;                                     //       LEDを消灯設定する
    colorWipe(strip.Color(y, y, y));
    count = 1;
  }
}
