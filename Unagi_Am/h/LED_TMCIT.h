#include <CircularLED.h>

CircularLED circularLED(6, 5);

void SenttocircularBar(int index) {
  for (int i=0;i<24;i++) {
    if (i<index) LED[i]=0xff;
    else         LED[i]=0;
  }
  circularLED.CircularLEDWrite(LED);
}
