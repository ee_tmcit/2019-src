#include "parameter.h"
#include "h/LED_TMCIT.h"

//#include <CircularLED.h>
//#include <Encoder.h>
#include <TimerOne.h>
//CircularLED circularLED(6, 5);

unsigned int LED[24];
//int index_LED;      //LED円板の表示数
int analogPin = 0; // アナログ入力端子番号(充電器に接続し電圧を読み取る)
//int division = 40;    //電圧をLED円板の表示数(index_LED)に変換幅

void setup(){
}

void loop(){
  
  int charge = analogRead(analogPin); // 充電回路から電圧を読み込む
  int index_LED = charge/division;    // 光らせるLEDの個数を仮決定
  
  //                                      LED個数がが24以上、０以下の場合の対応
  if (index_LED > 23)    index_LED = 23;
  else if(index_LED < 0) index_LED = 0;
      
  SenttocircularBar(index_LED);   //LEDをindex_LED個光らせる
  
}
