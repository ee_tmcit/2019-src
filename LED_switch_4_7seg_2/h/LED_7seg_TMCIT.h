#include "TM1637.h"

const int CLK_7seg_0 = 8;// 端子番号は小さくプリントされているもの
const int DIO_7seg_0 = 9;// 端子番号は小さくプリントされているもの

const int CLK_7seg_1 = 10;// 端子番号は小さくプリントされているもの
const int DIO_7seg_1 = 11;// 端子番号は小さくプリントされているもの

TM1637 tm1637_0(CLK_7seg_0, DIO_7seg_0);
TM1637 tm1637_1(CLK_7seg_1, DIO_7seg_1);
