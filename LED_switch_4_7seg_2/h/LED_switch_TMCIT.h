#include "Arduino.h"
#include "../parameter.h"

const int do_now=1;
const int dont=0;

struct led {
  int ledPin;
  int buttonPin;
  int buttonState;
  int lastButtonState;
  int counter;
  int blinkFlag;
};

struct led led0 = {0, 1, 0, HIGH, 0, 0}; //端子：UART
struct led led1 = {2, 3, 0, HIGH, 0, 0}; //端子：D2
struct led led2 = {4, 5, 0, HIGH, 0, 0}; //端子：D4
struct led led3 = {6, 7, 0, HIGH, 0, 0}; //端子：D6
//struct led led4 = {8, 9, 0, HIGH, 0, 0}; //端子：D8

// チャタリング防止用変数
unsigned long lastDebounceTime = 0;
unsigned long debounceDelay = 20;  // この値を大きくすると長いチャタリングにも対応できる。

void initLEDSW(struct led *l) {
  pinMode(l->buttonPin, INPUT);
  pinMode(l->ledPin, OUTPUT);
  digitalWrite(l->ledPin, HIGH);
}

void readSW(struct led *l, int reading) {//スイッチの状態変化を検出し計上する関数

  if (reading != l->lastButtonState) {// スイッチが閉じたか開いた場合
    lastDebounceTime = millis();      // 　その時間を記録
  }
 
  if ((millis() - lastDebounceTime) > debounceDelay) { // 十分長い時間、スイッチが閉じていたか、開いていた場合
    if (reading != l->buttonState) {                   // 　スイッチ状態が変化していた場合
      l->buttonState = reading;                        // 　　スイッチ状態を更新
      l->blinkFlag = do_now;                           // 　　点滅のためのフラグを立てる
      if (l->buttonState == LOW) {                     // 　　スイッチが閉じている時（押されている時）
        l->counter++;                                  // 　　  押された数を計上
      }
      //else {                                         // 　　スイッチが開いている時（何もされていない時）
      //}
    }
  }




}

void blinkLED(struct led *l, int reading) {      // LEDを点滅させる関数
  if (l->blinkFlag == do_now) {
    for(int i=0; i < ledBlinkStep; i++) {
      digitalWrite(l->ledPin, LOW);
      delay(ledBlinkWait);
      digitalWrite(l->ledPin, HIGH);
      delay(ledBlinkWait);
    }
    l->blinkFlag = dont;
  }
  l->lastButtonState = reading;
}
