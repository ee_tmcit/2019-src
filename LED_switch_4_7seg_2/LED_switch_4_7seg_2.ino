#include "h/LED_switch_TMCIT.h"
#include "h/LED_7seg_TMCIT.h"

/* 回路接続
 * 　LEDスイッチ0：Grove UART端子
 * 　LEDスイッチ1：Grove D2端子
 * 　LEDスイッチ2：Grove D4端子
 * 　LEDスイッチ3：Grove D6端子
 * 
 * 　7seg LED0：Grove D8端子
 * 　7seg LED1：GND（黒）, VCC（赤）, 11（白）, 10（黄）　一本ずつ接続
 */

/* 動作概要
 * 　LEDスイッチを押すと、そのスイッチが押された回数が記録される。
 * 　その回数を 7seg LED 2桁にに表示する。
 * 　押した直後、スイッチの LED が点滅する。
 * 　マイコンをリセットすると記録は消える。
 * 　以上の制御を、4組の「スイッチと7seg 」に対して個別に行う。
 * 　7seg とスイッチの組み合わせは以下の通り
 * 　　　7seg 0：上2桁はスイッチ1に、下2桁はスイッチ0に対応
 * 　　　7seg 1：上2桁はスイッチ3に、下2桁はスイッチ2に対応
 *
 *   スイッチが押されている/何もされていないときに処理をしたい場合は LED_switch_TMCIT.h を編集せよ。
 */

/* 動作パラメータ
 *   ledBlinkStep = 5;   //点滅回数（デフォルト：5回、実際はこの倍点滅する）
 *   ledBlinkWait = 100; //点滅周期（デフォルト：100[ms]、大きいほど遅い、実際はこの倍）
 *
 *   これらを変更するにはファイル parameter.h を編集せよ。
 */


void setup() {
  
  initLEDSW(&led0); // LEDスイッチの初期化
  initLEDSW(&led1);
  initLEDSW(&led2);
  initLEDSW(&led3);

  tm1637_0.init(); //7seg LEDの初期化
  tm1637_0.set(BRIGHT_TYPICAL);//BRIGHT_TYPICAL=2,BRIGHT_DARKEST=0,BRIGHTEST=7;
  tm1637_1.init();
  tm1637_1.set(BRIGHT_TYPICAL);

  //Serial.begin(115200); // デバッグ用シリアルモニタ
  
}

void loop() {
  int reading;

  reading = digitalRead(led0.buttonPin); // スイッチ0 の読み込みと点滅
  readSW(&led0, reading);
  tm1637_0.displayNum(100*led1.counter+led0.counter); // LED0 の表示 (上2桁：led1、下2桁：led0)
  blinkLED(&led0, reading);
  if(led0.counter == 100) led0.counter=99;

  reading = digitalRead(led1.buttonPin); // スイッチ1 の読み込みと点滅
  readSW(&led1, reading);
  tm1637_0.displayNum(100*led1.counter+led0.counter); // LED0 の表示 (上2桁：led1、下2桁：led0)
  blinkLED(&led1, reading);
  if(led1.counter == 100) led1.counter=99;

  reading = digitalRead(led2.buttonPin); // スイッチ2 の読み込みと点滅
  readSW(&led2, reading);
  tm1637_1.displayNum(100*led3.counter+led2.counter); // LED1 の表示 (上2桁：led3、下2桁：led2)
  blinkLED(&led2, reading);
  if(led2.counter == 100) led2.counter=99;

  reading = digitalRead(led3.buttonPin); // スイッチ3 の読み込みと点滅
  readSW(&led3, reading);
  tm1637_1.displayNum(100*led3.counter+led2.counter); // LED1 の表示 (上2桁：led3、下2桁：led2)
  blinkLED(&led3, reading);
  if(led3.counter == 100) led3.counter=99;



 
  //Serial.print(led0.counter); // デバッグ用シリアルモニタ
  //Serial.print(",");
  //Serial.print(led1.counter);
  //Serial.print(",");
  //Serial.print(led2.counter);
  //Serial.print(",");
  //Serial.print(led3.counter);
  //Serial.print(",");
  //Serial.print(led4.counter);
  //Serial.println(" ");
  //delay(100);
}
