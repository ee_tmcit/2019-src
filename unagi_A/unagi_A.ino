#include "parameter.h"
#include "h/LED_TMCIT.h"
#include "h/analog_TMCIT.h"

/* 回路接続
 *  
 * 　　充電回路：
 *           GND: Arduino GND
 *           出力端子：Arduino A0端子
 * 
 * 　　LED 円板：
 * 　　　　　　Arduino Grove D5端子
 * 
 */



/* 動作概要
 *  
 *  充電回路の出力電圧に応じた個数のLEDが点灯する。
 * 
 */


 
/* 動作パラメータ
 *  
 *  int division = 40;    //電圧の分割数
 *                        //LEDの点灯数が多すぎる場合この数値を大きくする
 *                        //LEDの点灯数が少なすぎる場合この数値を小さくする
 *   
 *   これらを変更するにはファイル parameter.h を編集せよ。
 */ 



void setup(){
}

void loop(){

  int charge = analogRead(analogPin); // 充電回路から電圧を読み込む
  int LEDn = charge/division;    // 光らせるLEDの個数を仮決定
  
  //                                      LED個数がが24以上、０以下の場合の対応
  if (LEDn > 23)    LEDn = 23;
  else if(LEDn < 0) LEDn = 0;
      
  SenttocircularBar(LEDn);   //LEDをLEDn個光らせる
  
}
