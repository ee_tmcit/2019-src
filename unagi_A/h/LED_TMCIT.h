#include <CircularLED.h>

#define LED_DATA 6 // LEDのデータ端子はD6に
#define LED_CLK  5 // LEDのクロック端子はD5

CircularLED circularLED(LED_DATA, LED_CLK);

unsigned int LED[24];


void SenttocircularBar(int index) {
  for (int i=0;i<24;i++) {
    if (i<index) LED[i]=0xff;
    else LED[i]=0;
  }
  circularLED.CircularLEDWrite(LED);
}
