#include "parameter.h"
#include "unagiB.h"


/* 回路接続
 * 　　人感センサ：
 *           Grove D4端子
 *           
 * 　　MP3プレイヤ：
 * 　　　　　　Grove D2端子   
 * 
 * 　　LEDテープ：
 * 　　　　　　　　　黒：外部電源のGND端子
 * 　　　　　　　　　緑：arduino D6端子
 *                赤：外部電源の5V     
 * 
 * 　　外部電源：5V
 * 　　　　　　　　　GND：ArduinoのGND端子（とLEDテープのGND）
 *                5V：LEDの5V端子         
 */



/* 動作概要
 *  
 *  人感センサが反応したら、LEDが点灯し、MP3プレイヤが再生する。
 * 
 */


 
/* 動作パラメータ
 *  
 *  #define MAX_VAL 64  // 明るさ（0 - 255）  
 *  #define LEDn 15     // 光らせるLEDの数/アクリル1枚あたり
 *   
 *  これらを変更するにはファイル parameter.h を編集せよ。
 *   
 *  色のカスタマイズをするにはファイル 
 *  
 */ 




void loop() {
//----- motion sensor -----
    if(digitalRead(PIR_MOTION_SENSOR)){//if it detects the moving people?
        //Serial.println("Hi,people is coming");
        Mp3Player.play(); //mp3プレーヤーの音を鳴らす
        actionLED();  //LEDテープを光らせる
    } else {
        //Serial.println("Watching");
        rainbow_off();  //LEDテープの光を消す
    }
    delay(200);
}
