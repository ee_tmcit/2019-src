//----- mp3 player -----
#include "KT403A_Player.h"
#include "KT403_myheader.h"

//----- motion sensor -----
#define PIR_MOTION_SENSOR 4

//----- LED Tape -----
#include <Adafruit_NeoPixel.h>
//#define MAX_VAL 64  // 0 to 255 for brightness
#define DELAY_TIME 2
#define DELAY_TIME2 50
#define LEDpin 6
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LEDn*4, LEDpin, NEO_GRB + NEO_KHZ800);





void setup() {
    //----- mp3 player -----
    ShowSerial.begin(9600);
    COMSerial.begin(9600);
    while (!ShowSerial);
    while (!COMSerial);
    Mp3Player.init(COMSerial);
    
    //----- motion sensor -----
    pinMode(PIR_MOTION_SENSOR, INPUT);
    Serial.begin(9600);
    
    //----- LED Tape -----
    strip.begin();
    strip.show(); // Initialize all pixels to 'off'
}
