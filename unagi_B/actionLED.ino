// Some example procedures showing how to display to the pixels:
void actionLED(void) {
  colorWipe2(strip.Color(MAX_VAL, 0, 0), DELAY_TIME,                0,   LEDn-1); // Red
  colorWipe2(strip.Color(0, MAX_VAL, 0), DELAY_TIME,              LEDn, 2*LEDn-1); // Green
  colorWipe2(strip.Color(MAX_VAL, MAX_VAL, 0), DELAY_TIME,      2*LEDn, 3*LEDn-1); // Yellow
  colorWipe2(strip.Color(MAX_VAL, MAX_VAL, MAX_VAL), DELAY_TIME,3*LEDn, 4*LEDn-1); // White
  //
  //  色のカスタマイズの仕方
  //　　　　　　　　　Color関数の引数を変える（0 - 255）
  //               Color関数の引数：赤成分、緑成分、青成分
  //
  //colorWipe(strip.Color(0, 0, 0), DELAY_TIME); // Black
  //colorWipe(strip.Color(0, 0, MAX_VAL), DELAY_TIME); // Blue
  //colorWipe(strip.Color(MAX_VAL, 0, MAX_VAL), DELAY_TIME); // Purple
  //colorWipe(strip.Color(0, MAX_VAL, MAX_VAL), DELAY_TIME); // Cyan
  //rainbow(DELAY_TIME2);
  //rainbowCycle(DELAY_TIME2);
}
