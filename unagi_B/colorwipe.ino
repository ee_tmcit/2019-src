// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  //for(uint16_t i=0; i<strip.numPixels(); i++) {
  for(uint16_t i=0; i<100; i++) {
    //Serial.println(i);
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);
  }
}

void colorWipe2(uint32_t c, uint8_t wait, uint8_t startLED, uint8_t endLED) {
  //Serial.println(c);
  for(uint16_t i=startLED; i<endLED; i++) {
    //Serial.println(i);
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);
  }
}
