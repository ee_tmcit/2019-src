//----- mp3 player -----
#include "KT403A_Player.h"
#include "KT403_myheader.h"

//----- motion sensor -----
#define PIR_MOTION_SENSOR 4//Use pin 2 to receive the signal from the module

//----- LED Tape -----
#include <Adafruit_NeoPixel.h>
#define MAX_VAL 64  // 0 to 255 for brightness
#define DELAY_TIME 2
#define DELAY_TIME2 50
Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, 6, NEO_GRB + NEO_KHZ800);

void setup() {
    //----- mp3 player -----
    ShowSerial.begin(9600);
    COMSerial.begin(9600);
    while (!ShowSerial);
    while (!COMSerial);
    Mp3Player.init(COMSerial);
    
    //----- motion sensor -----
    pinMode(PIR_MOTION_SENSOR, INPUT);
    Serial.begin(9600);
    
    //----- LED Tape -----
    strip.begin();
    strip.show(); // Initialize all pixels to 'off'
}
