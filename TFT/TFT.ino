#include "parameter.h"
#include "h/TFT_TMCIT.h"
#include <Wire.h>

/* 回路接続
 *  
 * ■ Arduino 0 (master)の回路
 * 
 * 　TFT液晶 GND：GND
 *          3V3：3.3V
 *          CS：A3
 *          RS：A2
 *          WR：A1
 *          RD：A0
 *          RST：RESET
 *          LED：GND
 *          DB0：D8
 *          DB1：D9
 *          DB2：D10
 *          DB3：D11
 *          DB4：D4
 *          DB5：D13
 *          DB6：D6
 *          DB7：D7
 *
 * 　Arduino 1：https://qiita.com/hikoalpha/items/7bf563ee286a59bfd2f4
 *          A4：A4 (I2C:SDA)
 *          A5：A5 (I2C:SCL)
 *         GND：GND
 *  
 * ■ Arduino 1 (slave)の回路
 * 
 * 　スイッチ0：抵抗0=D0端子、GND端子（抵抗でプルアップ、ボタンを押した時LOW）UARTは使用できない
 * 　スイッチ1：抵抗1=D1端子、GND端子（抵抗でプルアップ、ボタンを押した時LOW）UARTは使用できない
 * 　スイッチ2：抵抗2=D2端子、GND端子（抵抗でプルアップ、ボタンを押した時LOW）
 * 　スイッチ3：抵抗3=D3端子、GND端子（抵抗でプルアップ、ボタンを押した時LOW）
 * 
 * 　抵抗0：スイッチ0=D0端子、VCC端子（プルアップ）
 * 　抵抗1：スイッチ1=D1端子、VCC端子（プルアップ）
 * 　抵抗2：スイッチ2=D2端子、VCC端子（プルアップ）
 * 　抵抗3：スイッチ3=D3端子、VCC端子（プルアップ）
 *
 * 　Arduino 0
 *          A4：A4 (I2C:SDA)
 *          A5：A5 (I2C:SCL)
 *         GND：GND
 * 
 * 　    　 サーボモータ0：D5端子 (PWM可能端子)
 * 　    　 サーボモータ1：D6端子 (PWM可能端子)
 * 　     　サーボモータ2：D9端子 (PWM可能端子)
 * 　     　サーボモータ3：D10端子(PWM可能端子)
 */

/* 動作概要
 * 　平時はモニターに「Select 2 Colors」と表示。
 * 　平時はスイッチがインク容器に押されている。容器を取るとスイッチが開く。
 * 　2つのスイッチが開く（=容器が取り上げられる）と以下の処理が起きる。
 * 　　・（取り上げた2つのインクに相当する）2色を液晶に表示（背景色、文字色）。
 * 　　・（取り上げたインクに応じて）サーボモータを動かし容器の蓋を空ける（香り発生）
 * 　スイッチが戻る（=容器を元に戻す）と、平時に戻る。
 */

/* 動作パラメータ
 *  #define clr_0 0x001F：　インク0の色
 *  #define clr_1 0xF800：　インク1の色
 *  #define clr_2 0x07E0：　インク2の色
 *  #define clr_3 0xFFE0：　インク3の色
 *  
 *  const char* text0 = "Select 2 Colors"; // アイドル時の表示
 *  const char* text1 = "TMCIT-EEEP";      // 2色選ばれている時の表示
 *  
 *  const int x = 75;          // 文字の表示位置 x
 *  const int y = 100;         // 文字の表示位置 y
 *  const int orientation = 1; // 文字の表示方向（0-3）
 *
 *   これらを変更するにはファイル parameter.h を編集せよ。
 */

/* 注意：
 * 　　　以下をしないと動きません
 * 　　　　
 * 　　　・ファイル arduino ソースフォルダ/libraries/MCUFRIEND_kbv-master/MCUFRIEND_kbv.cpp　　
 * 　　　　の19行目
 * 　　　　           #define SUPPORT_9326_5420
 * 　　　　のコメントをはずす。
 * 　　　・フォントファイル Font/Mountains_of_Christmas_Bold_48pt.h を以下のフォルダに配置
 * 　　　　arduino IDE のソースフォルダ/libraries/Adafruit-GFX-Library-master/Fonts
 */



const int slave_ID = 0; //ここを変えたら slave 側のソースコードも変えること





int Screen;

void setup(void) {
  
  uint16_t ID = tft.readID();
  tft.begin(ID);    //ID = 0x9326;
  tft.setFont(&Mountains_of_Christmas_Bold_48);

  //initSwitch(switch0, switch1, switch2, switch3);
  
  Screen = Not_Keep; //Not_Keep のときのみ画面表示を切り替える

  Wire.begin();
  Serial.begin(9600);
    
}





void loop(void) {
  Wire.requestFrom(slave_ID, 1);
  while (Wire.available()) {
    byte sw = Wire.read();

                           // 反応しているスイッチのチェック（1の時：反応している）
    int sw3 = sw/16;
    int sw2 = (sw%16)/8;
    int sw1 = (sw%8)/4;
    int sw0 = (sw%4)/2;

    
    //Serial.print(sw, ":");
    Serial.print("16:");
    Serial.print(sw3);
    Serial.print(", 8:");
    Serial.print(sw2);
    Serial.print(", 4:");
    Serial.print(sw1);
    Serial.print(", 2:");
    Serial.println(sw0);

    unsigned int colors[2];// 表示する2色
    int nCol = 0;          // 反応しているスイッチの個数（3までしか計上しない） 
 
    if(sw0==1) {            //スイッチ0が反応した時
      colors[nCol] = clr_0;
      nCol++;
    }
  
    if(sw1==1) {            //スイッチ1が反応した時
      colors[nCol] = clr_1;
      nCol++;
    }
  
    if(nCol<2 && sw2==1) {  //スイッチ2が反応した時
      colors[nCol] = clr_2;
      nCol++;
    }
  
    if(nCol<2 && sw3==1) {  //スイッチ3が反応した時
      colors[nCol] = clr_3;
      nCol++;
    }


    if(nCol==2) {                             // 2つのスイッチだけが押されていない時
      paintText(colors[0], colors[1], x, y, orientation, text1);
      delay(2000);
      paintText(colors[1], colors[0], x, y, orientation, text1);
      delay(2000);
      Screen = Not_Keep;
    
    } else if(Screen==Not_Keep) {             // 全てのスイッチが押されている時（アイドル時）
      paintText(BLACK, WHITE, x, y, orientation, text0);
      Screen = Keep;
    
    }

  }


  // 以下は液晶のドライバについていたサンプルプログラム
  // 様々な表示方法がある

  /*

    uint8_t aspect;
    uint16_t pixel;
    const char *aspectname[] = {
        "PORTRAIT", "LANDSCAPE", "PORTRAIT_REV", "LANDSCAPE_REV"
    };
    const char *colorname[] = { "BLUE", "GREEN", "RED", "GRAY" };
    uint16_t colormask[] = { 0x001F, 0x07E0, 0xF800, 0xFFFF };
    uint16_t dx, rgb, n, wid, ht, msglin;
    tft.setRotation(0);
    //runtests();
  
    delay(2000);
    if (tft.height() > 64) {
        for (uint8_t cnt = 0; cnt < 4; cnt++) {
            aspect = (cnt + 0) & 3;
            tft.setRotation(aspect);
            wid = tft.width();
            ht = tft.height();
            msglin = (ht > 160) ? 200 : 112;
            testText();
            dx = wid / 32;
            for (n = 0; n < 32; n++) {
                rgb = n * 8;
                rgb = tft.color565(rgb, rgb, rgb);
                tft.fillRect(n * dx, 48, dx, 63, rgb & colormask[aspect]);
            }
            tft.drawRect(0, 48 + 63, wid, 1, WHITE);
            tft.setTextSize(2);
            tft.setTextColor(colormask[aspect], BLACK);
            tft.setCursor(0, 72);
            tft.print(colorname[aspect]);
            tft.setTextColor(WHITE);
            tft.println(" COLOR GRADES");
            tft.setTextColor(WHITE, BLACK);
            printmsg(184, aspectname[aspect]);
            delay(1000);
            tft.drawPixel(0, 0, YELLOW);
            pixel = tft.readPixel(0, 0);
            tft.setTextSize((ht > 160) ? 2 : 1); //for messages
#if defined(MCUFRIEND_KBV_H_)
#if 1
            extern const uint8_t penguin[];
            tft.setAddrWindow(wid - 40 - 40, 20 + 0, wid - 1 - 40, 20 + 39);
            tft.pushColors(penguin, 1600, 1);
#elif 1
            extern const uint8_t wifi_full[];
            tft.setAddrWindow(wid - 40 - 40, 20 + 0, wid - 40 - 40 + 31, 20 + 31);
            tft.pushColors(wifi_full, 1024, 1, true);
#elif 1
            extern const uint8_t icon_40x40[];
            tft.setAddrWindow(wid - 40 - 40, 20 + 0, wid - 1 - 40, 20 + 39);
            tft.pushColors(icon_40x40, 1600, 1);
#endif
            tft.setAddrWindow(0, 0, wid - 1, ht - 1);
            if (aspect & 1) tft.drawRect(wid - 1, 0, 1, ht, WHITE);
            else tft.drawRect(0, ht - 1, wid, 1, WHITE);
            printmsg(msglin, "VERTICAL SCROLL UP");
            uint16_t maxscroll;
            if (tft.getRotation() & 1) maxscroll = wid;
            else maxscroll = ht;
            for (uint16_t i = 1; i <= maxscroll; i++) {
                tft.vertScroll(0, maxscroll, i);
                delay(10);
            }
            delay(1000);
			printmsg(msglin, "VERTICAL SCROLL DN");
            for (uint16_t i = 1; i <= maxscroll; i++) {
                tft.vertScroll(0, maxscroll, 0 - (int16_t)i);
                delay(10);
            }
			tft.vertScroll(0, maxscroll, 0);
            printmsg(msglin, "SCROLL DISABLED   ");

            delay(1000);
            if ((aspect & 1) == 0) { //Portrait
                tft.setTextColor(BLUE, BLACK);
                printmsg(msglin, "ONLY THE COLOR BAND");
                for (uint16_t i = 1; i <= 64; i++) {
                    tft.vertScroll(48, 64, i);
                    delay(20);
                }
                delay(1000);
            }
#endif
            tft.setTextColor(YELLOW, BLACK);
            if (pixel == YELLOW) {
                printmsg(msglin, "SOFTWARE SCROLL    ");
#if 0
                // diagonal scroll of block
                for (int16_t i = 45, dx = 2, dy = 1; i > 0; i -= dx) {
                    windowScroll(24, 8, 90, 40, dx, dy, scrollbuf);
                }
#else
                // plain horizontal scroll of block
                n = (wid > 320) ? 320 : wid;
                for (int16_t i = n, dx = 4, dy = 0; i > 0; i -= dx) {
                    windowScroll(0, 200, n, 16, dx, dy, scrollbuf);
                }
#endif
            }
            else if (pixel == CYAN)
                tft.println("readPixel() reads as BGR");
            else if ((pixel & 0xF8F8) == 0xF8F8)
                tft.println("readPixel() should be 24-bit");
            else {
                tft.print("readPixel() reads 0x");
                tft.println(pixel, HEX);
            }
            delay(5000);
        }
    }
    printmsg(msglin, "INVERT DISPLAY ");
    tft.invertDisplay(true);
    delay(2000);
    tft.invertDisplay(false);

  */



    
}
