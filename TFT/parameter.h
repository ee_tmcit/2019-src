//#define	BLACK   0x0000
//#define	BLUE    0x001F
//#define	RED     0xF800
//#define	GREEN   0x07E0
//#define CYAN    0x07FF
//#define MAGENTA 0xF81F
//#define YELLOW  0xFFE0
//#define WHITE   0xFFFF


//BLUE
#define clr_0 0x001F
//RED
#define clr_1 0xF800
//GREEN
#define clr_2 0x07E0
//YELLOW
#define clr_3 0xFFE0


const char* text0 = "Select 2 Colors"; // アイドル時の表示
const char* text1 = "TMCIT-EEEP";      // 2色選ばれている時の表示

const int x = 75;          // 文字の表示位置 x
const int y = 100;         // 文字の表示位置 y
const int orientation = 1; // 文字の表示方向
