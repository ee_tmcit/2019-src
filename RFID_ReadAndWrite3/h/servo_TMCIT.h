#include <Servo.h>

#define sv0_pin 4
#define sv1_pin 5

Servo sv0;
Servo sv1;


void initServo() {
  sv0.attach(sv0_pin);  // サーボモータ0の初期化
  sv1.attach(sv1_pin);  // サーボモータ1の初期化
}
