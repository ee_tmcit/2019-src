#include <SPI.h>
#include <MFRC522.h>

#define RST_PIN   9
#define SS_PIN   10

#define SAME      0
#define DIFFERENT 1

MFRC522 mfrc522(SS_PIN, RST_PIN);

MFRC522::MIFARE_Key key;

byte CorrectKEY[] = {0x07, 0x73, 0x8E, 0x43};

byte dataLock[]    = {
        0x00, 0x01, 0x02, 0x03,
        0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,
        0x0c, 0x0e, 0x0e, 0x0f
    };

byte dataUnlock[]    = {
        0x0f, 0x0e, 0x0d, 0x0c,
        0x0b, 0x0a, 0x09, 0x08,
        0x07, 0x06, 0x05, 0x04,
        0x03, 0x02, 0x01, 0x00
    };


byte sector         = 1;
byte blockAddr      = 4;
byte trailerBlock   = 7;
MFRC522::StatusCode status;
byte buffer[18];
byte size = sizeof(buffer);








int compareByte(byte* array1, byte* array2, int sizeOfArray) {
  int flag = SAME;
  for(int i=0; i<sizeOfArray; i++) {
     if(array1[i]!=array2[i]) {
      flag = DIFFERENT;
      return flag;
     }
  }
  return flag;
}


void initRFID() {
    SPI.begin();          // SPI の初期化
    mfrc522.PCD_Init();   // RFIDリーダの初期化

    for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
    }
}


// Check for compatibility
void checkCompatibility() {
    MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    if (    piccType != MFRC522::PICC_TYPE_MIFARE_MINI
        &&  piccType != MFRC522::PICC_TYPE_MIFARE_1K
        &&  piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
        return;
    }
}



// Authenticate using key A
void authenticate() {
  status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) return;
}


void finish() {
  // Halt PICC
  mfrc522.PICC_HaltA();
  // Stop encryption on PCD
  mfrc522.PCD_StopCrypto1();
}
