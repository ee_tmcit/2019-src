

#include "parameter.h"
#include "h/servo_TMCIT.h"
#include "h/RFID_TMCIT.h"

/* 回路接続
 * 　　RFID：
 *           MFRC522      Arduino
 *              RST           9
 *              SDA(SS)      10
 *              MOSI         11
 *              MISO         12
 *              SCK          13
 *              GND          GND
 *              3.3          3.3
 * 
 * 　　サーボモータ0：
 * 　　　　　　　　　橙線：外部電源のGND
 * 　　　　　　　　　赤線：外部電源の5V
 * 　　　　　　　　　黄線：arduino D4
 * 　　サーボモータ1：
 * 　　　　　　　　　橙線：外部電源のGND
 * 　　　　　　　　　赤線：外部電源の5V
 * 　　　　　　　　　黄線：arduino D5
 * 
 * 　　外部電源：
 *                5V：両サーボモータの赤線
 *               GND：arduino の GND（と両サーボモータのGND）
 */

/* 動作概要
 * 　カード型のキーをRFIDリーダに近づけると
 * 　ロックからアンロックまたはアンロックからロックへと状態変化する。
 * 　丸いキーを近づけても何も反応しない。
 * 　ロック時にはサーボモータがロック位置まで回転し
 * 　アンロック時にはサーボモータがアンロック位置まで回転する。
 * 　両モータとも同じ角度で回転する。
 */
 
/* 動作パラメータ
 *   #define LockAngle    45 // ロック角
 *   #define UnlockAngle 135 // アンロック角
 *   
 *   これらを変更するにはファイル parameter.h を編集せよ。
 */ 


void setup() {
  
    //Serial.begin(9600); // デバッグ用
    //while (!Serial);    // デバッグ用
    
    initRFID();
    initServo();
}


void loop() {

    if ( ! mfrc522.PICC_IsNewCardPresent()) return;
    if ( ! mfrc522.PICC_ReadCardSerial()) return;
    
    if(compareByte(mfrc522.uid.uidByte,CorrectKEY,4)) { // 正しい鍵ではなかった場合
      //Serial.println("not correct key"); // デバッグ用
      return;
    }

    checkCompatibility();
    authenticate();

    
 
    // カードに書き込み
    status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(blockAddr, buffer, &size);
    if( compareByte(buffer,dataLock,16) != SAME) {                                 // カードに lock と記録されていない場合
      status = (MFRC522::StatusCode) mfrc522.MIFARE_Write(blockAddr, dataLock, 16);
      sv0.write(LockAngle);                                                               // サーボモータをロック角度に
      sv1.write(LockAngle);
      delay(1000);
      //Serial.println("Locked");
    } else {                                                                       // カードに lock と記録されていた場合
      status = (MFRC522::StatusCode) mfrc522.MIFARE_Write(blockAddr, dataUnlock, 16);
      sv0.write(UnlockAngle);                                                               // サーボモータをアンロック角度に
      sv1.write(UnlockAngle);
      delay(1000);
      //Serial.println("Un-Locked");
    }



    finish();
}
