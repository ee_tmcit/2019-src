/*macro definitions of PIR motion sensor pin and LED pin*/
#include <Adafruit_NeoPixel.h>
#include "Grove_I2C_Motor_Driver.h"
#include <Wire.h>
#include "paj7620.h"

#define LED_PIN 6           // LED のピンは D6 に

#define MOTOR_ADDRESS 0x0f  // モータドライバはI2Cバスに接続。ディップスイッチは全て上のまま(0x0f)

#define GES_REACTION_TIME       500
#define GES_ENTRY_TIME          500
#define GES_QUIT_TIME           1000

Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_NUM, LED_PIN, NEO_GRB + NEO_KHZ800);




void colorWipe(uint32_t c) {
  for (uint16_t i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
  }
  strip.show();
}
