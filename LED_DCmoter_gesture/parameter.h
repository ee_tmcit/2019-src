#define LED_NUM            10 // LED の個数

#define MOTOR_SPEED       100 // モータスピード

const int colorR = 255; // LEDの点灯色：赤[0から255]
const int colorG = 255; // LEDの点灯色：緑[0から255]
const int colorB = 255; // LEDの点灯色：青[0から255]
const int pTime = 1000;

const int tcMac = 100;  // 自動スイッチオフまでの時間（100で約10秒）
