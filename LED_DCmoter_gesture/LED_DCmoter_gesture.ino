#include "parameter.h"
#include "h/LED_DCmotor_gesture_TMCIT.h"

/* 回路接続
 * 　　　　LEDテープ：
 * 　　　　　　　　　信号線：Arduino D6端子
 * 　　　　　　　　　電源線：外部電源5V
 * 　　　　　　　　　GND：  Arduino GND、外部電源 GND
 * 　　モータドライバ：
 * 　　　　　　　　　I2C：Arduino Grove I2C端子
 * 　　　　　　　　　M1+：DCモータ
 * 　　　　　　　　　M1-：DCモータ
 * 　　　　　　　　　外部電源用VS：外部電源3V
 * 　　　　　　　　　外部電源用GND：外部電源GND
 * 　ジェスチャセンサ：Grove I2C端子
 * 　　　　　　モータ：モータドライバの M1+, M1-
 * 　　　　　外部電源：5Vと3Vが必要（レギュレータで）
 */

/* 動作概要
 * 　以下のジェスチャーに応じて、モータが正転・逆転・停止、LEDが点灯・消灯する。
 * 　・上：LED点灯
 * 　・下：LED、モータ共に停止
 * 　・時計回り：モータが正転
 * 　・半時計回り：モータが逆転
 * 　動作をはじめて一定時間が過ぎると全停止する。
 */
 
/* 動作パラメータ
 *   #define LED_NUM      10 // LED の個数
 *   #define MOTOR_SPEED 100 // モータスピード
 *   const int colorR = 255; // LEDの点灯色：赤[0から255]
 *   const int colorG = 255; // LEDの点灯色：緑[0から255]
 *   const int colorB = 255; // LEDの点灯色：青[0から255]
 *   const int pTime = 1000;
 *   const int tcMac = 100;  // 自動スイッチオフまでの時間（100で約10秒）
 *   
 *   これらを変更するにはファイル parameter.h を編集せよ。
 */ 

int timeCounter = 0;

void setup() {
  //Serial.begin(9600);          // デバッグ用シリアルモニタ
  
  Motor.begin(MOTOR_ADDRESS);    //モータドライバの初期化
  strip.begin();                 // LEDの初期化
  strip.show();
  uint8_t error = paj7620Init(); // ジェスチャーセンサを初期化  
}



void loop() {
    uint8_t inputGesture = 0;
    uint8_t inputGesture_2 = 0;
    uint8_t error = paj7620ReadReg(0x43, 1, &inputGesture);          // ジェスチャーを読む
    
    //Serial.println(timeCounter);// デバッグ用
    
    if (!error) {
        switch (inputGesture) {
            case GES_RIGHT_FLAG:
                delay(GES_ENTRY_TIME);
                paj7620ReadReg(0x43, 1, &inputGesture);
                if(inputGesture == GES_FORWARD_FLAG) {                 // 近づいてくる時
                    //Serial.println("Forward");// デバッグ用
                    delay(GES_QUIT_TIME);
                } else if(inputGesture == GES_BACKWARD_FLAG) {         // 離れていく時
                    //Serial.println("Backward");// デバッグ用
                    delay(GES_QUIT_TIME);
                } else {                                               // 右に動く時
                    //Serial.println("Right");// デバッグ用
                }         
                timeCounter = 0;
                break;
            case GES_LEFT_FLAG:
                delay(GES_ENTRY_TIME);
                paj7620ReadReg(0x43, 1, &inputGesture);
                if(inputGesture == GES_FORWARD_FLAG) {                 // 近づいてくる時
                    //Serial.println("Forward");// デバッグ用
                    delay(GES_QUIT_TIME);
                } else if(inputGesture == GES_BACKWARD_FLAG) {         // 離れていく時
                    //Serial.println("Backward");// デバッグ用
                    delay(GES_QUIT_TIME);
                } else {                                               // 左に動く時
                    //Serial.println("Left");// デバッグ用
                }                 
                timeCounter = 0; 
                break;
            case GES_UP_FLAG:
                delay(GES_ENTRY_TIME);
                paj7620ReadReg(0x43, 1, &inputGesture);
                if(inputGesture == GES_FORWARD_FLAG) {                 // 近づいてくる時
                    //Serial.println("Forward");// デバッグ用
                    delay(GES_QUIT_TIME);
                } else if(inputGesture == GES_BACKWARD_FLAG) {         // 離れていく時
                    //Serial.println("Backward");// デバッグ用
                    delay(GES_QUIT_TIME);
                } else {                                               // 上に動く時
                    //Serial.println("Up");// デバッグ用
                    colorWipe(strip.Color(colorR, colorG, colorB));         // LED点灯
                }                  
                timeCounter = 0;
                break;
            case GES_DOWN_FLAG:
                delay(GES_ENTRY_TIME);
                paj7620ReadReg(0x43, 1, &inputGesture);
                if(inputGesture == GES_FORWARD_FLAG) {                 // 近づいてくる時
                    //Serial.println("Forward");// デバッグ用
                    delay(GES_QUIT_TIME);
                } else if(inputGesture == GES_BACKWARD_FLAG) {         // 離れていく時
                    //Serial.println("Backward");// デバッグ用
                    delay(GES_QUIT_TIME);
                } else {                                               // 下に動く時
                    //Serial.println("Down");// デバッグ用
                    colorWipe(strip.Color(0, 0, 0));                        // LED消灯
                    Motor.stop(MOTOR1);                                     // モータ停止
                }                  
                timeCounter = 0;
                break;
            case GES_FORWARD_FLAG:                                     // 近づいてくる時
                //Serial.println("Forward");// デバッグ用
                delay(GES_QUIT_TIME);         
                timeCounter = 0;
                break;
            case GES_BACKWARD_FLAG:                                    // 離れていく時
                //Serial.println("Backward");// デバッグ用
                delay(GES_QUIT_TIME);         
                timeCounter = 0;
                break;
            case GES_CLOCKWISE_FLAG:                                   // 時計回りの時
                //Serial.println("Clockwise");// デバッグ用
                Motor.speed(MOTOR1, MOTOR_SPEED);                          // モータが正転      
                timeCounter = 0;
                break;
            case GES_COUNT_CLOCKWISE_FLAG:                             // 反時計回りの時
                //Serial.println("anti-clockwise");// デバッグ用
                Motor.speed(MOTOR1, -MOTOR_SPEED);                         // モータが逆転    
                timeCounter = 0;
                break; 
            default:
                paj7620ReadReg(0x44, 1, &inputGesture_2);
                if (inputGesture_2 == GES_WAVE_FLAG) {                 // 波状の時
                    //Serial.println("wave");// デバッグ用         
                    timeCounter = 0;
                } else {
                  if(timeCounter > tcMac) {                            // 動作が始まって一定時間が過ぎた時
                    Motor.stop(MOTOR1);                                        // モータ停止
                    colorWipe(strip.Color(0, 0, 0));                           // LED消灯
                    timeCounter = 0;
                  } else {
                    timeCounter++;
                  }
                }
                break;
        }
    }
    delay(100);

}
