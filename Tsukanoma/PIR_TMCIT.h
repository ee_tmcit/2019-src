#define PIR_MOTION_SENSOR 2     // 人感センサのピン設定

void initPIR() {
    pinMode(PIR_MOTION_SENSOR, INPUT);  // 人感センサのポートのモード設定：入力モード
}
