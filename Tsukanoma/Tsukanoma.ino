//============================================
//
//　　　　　　　　　　　　束の間
//
//============================================
// 作成者：川崎 憲広（研究室550室）
// 作成日：2020/2/9
// 連絡先：norihiro@metro-cit.ac.jp

// 編集：石崎(2/13)

/* 回路接続
 * 　　人感センサ：
 *           Grove D2端子
 * 
 * 　　サーボモータ：
 * 　　　　　　　　　黒線（端）：外部電源のGND
 * 　　　　　　　　　黒線（中）：外部電源の5V
 * 　　　　　　　　　白線：arduino D9端子
 * 
 * 　　LEDテープ：
 * 　　　　　　　　　GND：外部電源のGND
 * 　　　　　　　　　D：arduino D6端子
 *                5V：外部電源の5V
 */



/* 動作概要
 *  
 *  ①人感センサが反応したら、LEDを光らせる。
 *  ②かつ、サーボモータを「設定した角度angle」だけ回転させる往復動作を「往復回数N」だけ繰り返す。
 *  ③センサが無反応になったら、LEDとサーボモータを止める。
 */


 
/* 動作パラメータ
 *  
 *  int N=2;                        // サーボモータの往復回数N
 *  int angle=90;                   // 回転角度angle
 *  int lag=450;                    // サーボモータの待ち時間（制御に必要な時間を適切に設定する必要がある）
 *  
 *  int LEDn=3;                     // 点灯させるLEDの数
 *  #define MAX_VAL 64              // LEDの明るさ設定（0～255の範囲で設定する、255=最も明るい）
 *  int color_no=8;                 // LEDの色                                
 *                                  //1.Black, 2.Red, 3.Green, 4.Yellow, 5.Blue,
 *                                  // 6.Purple, 7.Cyan, 8.White, 9.rainbow, 10.rainbowCycle
 *   
 *   これらを変更するにはファイル parameter.h を編集せよ。
 */ 

#include "parameter.h"
#include "servo_TMCIT.h"
#include "LED_TMCIT.h"
#include "PIR_TMCIT.h"



//~~~~~~~~~~~~~~
// セットアップ
void setup(){

    initPIR();    // 人感センサ初期化
    initServo();  // サーボモータ初期化
    initLED();    // LEDテープ初期化
    
    // シリアルモニタの設定
    //Serial.begin(9600); // デバッグ用 
    
}




//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// メインループ（メインプログラム）
void loop()
{   
    //==============================
    // 人感センサのサンプルプログラム
    //==============================
    double Vout=digitalRead(PIR_MOTION_SENSOR);   // 人感センサの読み取り値の格納
    
    if(digitalRead(PIR_MOTION_SENSOR)){           // 人感センサが反応したら
        //Serial.println("人が来ました。");           // 「人が来ました。」とシリアルモニタに出力 // デバッグ用 
        
        //================================
        // サーボモータのサンプルプログラム
        //================================
        for(int i=0; i <= 2*N; i++){  // サーボモータをN往復させる
      
          myServo.write(0);           // サーボモータの位置：0[度]
          delay(lag);                 // 待ち時間：設定時間lag[ms]
          //Serial.println("angle: 0"); // シリアルモニタへ出力 // デバッグ用 

          myServo.write(angle);       // サーボモータの位置：設定角度：angle[度]
          delay(lag);                 // 待ち時間：設定時間lag[ms]
          //Serial.print("angle: ");    // シリアルモニタへ出力 // デバッグ用 
          //Serial.println(angle);      // シリアルモニタへ出力 // デバッグ用 

          //================================
          // LEDテープのサンプルプログラム
          //================================
          // LEDは様々な色で光ります。
          if(color_no==1){
            // 1. Black（消灯）
            colorWipe(strip.Color(0, 0, 0), DELAY_TIME);
          } else if(color_no==2){
            // 2. Red
            colorWipe(strip.Color(MAX_VAL, 0, 0), DELAY_TIME);
          } else if(color_no==3){
            // 3. Green
            colorWipe(strip.Color(0, MAX_VAL, 0), DELAY_TIME);
          } else if(color_no==4){
            // 4. Yellow
            colorWipe(strip.Color(MAX_VAL, MAX_VAL, 0), DELAY_TIME);
          } else if(color_no==5){
            // 5. Blue
            colorWipe(strip.Color(0, 0, MAX_VAL), DELAY_TIME);
          } else if(color_no==6){
            // 6. Purple
            colorWipe(strip.Color(MAX_VAL, 0, MAX_VAL), DELAY_TIME);
          } else if(color_no==7){
            // 7. Cyan
            colorWipe(strip.Color(0, MAX_VAL, MAX_VAL), DELAY_TIME);
          } else if(color_no==8){
            // 8. White
            colorWipe(strip.Color(MAX_VAL, MAX_VAL, MAX_VAL), DELAY_TIME); 
          } else if(color_no==9){
            // 9. rainbow
            rainbow(DELAY_TIME2);
          } else if(color_no==10){
            // 10. rainbowCycle
            rainbowCycle(DELAY_TIME2);
          }
        }
        
    } else {                                          // 人感センサが反応しなかったら
        //Serial.println("人はいません。");                // 「人はいません。」とシリアルモニタに出力 // デバッグ用 
        colorWipe(strip.Color(0, 0, 0), DELAY_TIME);  // LED消灯（Black）
        delay(200);
    }

    // Serial.println(Vout); // 人感センサの出力数値（確認用） // デバッグ用 


}
