int N=2;                        // サーボモータの往復回数N
int angle=90;                   // 回転角度angle
int lag=450;                    // サーボモータの待ち時間（制御に必要な時間を適切に設定する必要がある）

int LEDn=3;                     // 点灯させるLEDの数
#define MAX_VAL 64              // LEDの明るさ設定（0～255の範囲で設定する、255=最も明るい）
int color_no=8;                 // LEDの色
                                //1.Black, 2.Red, 3.Green, 4.Yellow, 5.Blue, 
                                // 6.Purple, 7.Cyan, 8.White, 9.rainbow, 10.rainbowCycle
